import { render, screen, fireEvent } from "@testing-library/react";
import App from "../src/App";

describe("Réponse de Bob", () => {
  it('doit répondre "Bien sûr." quand on lui pose une question en minuscules', () => {
    render(<App />);

    // Simuler l'entrée de l'utilisateur
    const input = screen.getByPlaceholderText("Entrez du texte ici");
    fireEvent.change(input, { target: { value: "Comment allez-vous ?" } });

    // Simuler le clic sur le bouton
    const button = screen.getByRole("button");
    fireEvent.click(button);

    // Vérifier la réponse de Bob
    const bobResponse = screen.getByText("Bien sûr.");
    expect(bobResponse).toBeInTheDocument();
  });
});