import { render, screen, fireEvent } from "@testing-library/react";
import App from "../src/App";

describe("Réponse de Bob", () => {

  it('doit répondre "Peu importe !" quand l\'utilisateur tape quelque chose qui ne correspond à aucun cas spécifique', async () => {
    render(<App />);

    // Simuler l'entrée de l'utilisateur avec un texte qui ne correspond à aucun cas spécifique
    const input = screen.getByPlaceholderText("Entrez du texte ici");
    fireEvent.change(input, { target: { value: "ADDsdsdsd" } });

    // Simuler le clic sur le bouton
    const button = screen.getByRole("button");
    fireEvent.click(button);

    // Vérifier la réponse de Bob
    const bobResponse = await screen.findByText("Peu importe !");
    expect(bobResponse).toBeInTheDocument();
  });
});