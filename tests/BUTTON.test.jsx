import { render, screen } from "@testing-library/react";
import App from "../src/App";

describe("UI test pour le formulaire", () => {
  it("doit afficher un bouton", () => {
    // 1. Rendre le composant App
    render(<App />);

    // 2. Vérifier que le bouton est présent
    const buttonElement = screen.getByRole("button");
    expect(buttonElement).toBeInTheDocument();
  });
});