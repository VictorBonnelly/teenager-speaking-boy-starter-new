import { render, screen } from "@testing-library/react";
import App from "../src/App";

describe("UI test pour le formulaire", () => {
  it("doit afficher un champ de saisie de texte", () => {
    // 1. Rendre le composant App
    render(<App />);

    // 2. Vérifier que le champ de saisie de texte est présent
    const inputElement = screen.getByRole("textbox");
    expect(inputElement).toBeInTheDocument();
  });
});