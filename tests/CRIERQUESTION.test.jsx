import { render, screen, fireEvent } from "@testing-library/react";
import App from "../src/App";

describe("Réponse de Bob", () => {
  it('doit répondre "Calmez-vous, je sais ce que je fais !" quand l\'utilisateur crie une question en majuscules', async () => {
    render(<App />);

    // Simuler l'entrée de l'utilisateur
    const input = screen.getByPlaceholderText("Entrez du texte ici");
    fireEvent.change(input, { target: { value: "CAVA ?" } });

    // Simuler le clic sur le bouton
    const button = screen.getByRole("button");
    fireEvent.click(button);

    // Vérifier la réponse de Bob
    const bobResponse = await screen.findByText("Calmez-vous, je sais ce que je fais !");
    expect(bobResponse).toBeInTheDocument();
  });
});