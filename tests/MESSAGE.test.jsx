import { render, screen } from "@testing-library/react";
import App from "../src/App";

describe("UI test pour le formulaire", () => {
  it("doit afficher une div avec l'identifiant 'bob'", () => {
    // 1. Rendre le composant App
    const { container } = render(<App />);

    // 2. Vérifier que la div avec l'identifiant 'bob' est présente
    const divElement = container.querySelector("#bob");
    expect(divElement).toBeInTheDocument();
  });
});