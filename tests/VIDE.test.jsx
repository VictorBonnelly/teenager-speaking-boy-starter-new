import { render, screen, fireEvent } from "@testing-library/react";
import App from "../src/App";

describe("Réponse de Bob", () => {
  

  it('doit répondre "Bien, on fait comme ça !" quand l\'utilisateur ne tape rien', async () => {
    render(<App />);

    // Simuler l'entrée de l'utilisateur avec une chaîne vide
    const input = screen.getByPlaceholderText("Entrez du texte ici");
    fireEvent.change(input, { target: { value: "" } });

    // Simuler le clic sur le bouton
    const button = screen.getByRole("button");
    fireEvent.click(button);

    // Vérifier la réponse de Bob
    const bobResponse = await screen.findByText("Bien, on fait comme ça !");
    expect(bobResponse).toBeInTheDocument();
  });
});